package ru.tsc.panteleev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.comparator.CreatedComporator;
import ru.tsc.panteleev.tm.comparator.DateBeginComporator;
import ru.tsc.panteleev.tm.comparator.NameComparator;
import ru.tsc.panteleev.tm.comparator.StatusComporator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status" , StatusComporator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComporator.INSTANCE),
    DATE_BEGIN("Sort by date begin", DateBeginComporator.INSTANCE);

    @Nullable
    private final String displayName;

    @Nullable
    private final Comparator comparator;

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort: values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(@Nullable final String displayName, @Nullable final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
