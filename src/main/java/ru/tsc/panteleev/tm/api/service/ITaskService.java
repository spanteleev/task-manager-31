package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(String userId, String projectId);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description,
                @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name,
                       @Nullable String description);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name,
                    @Nullable String description);

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
