package ru.tsc.panteleev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.ServerAboutRequest;
import ru.tsc.panteleev.tm.dto.request.ServerVersionRequest;
import ru.tsc.panteleev.tm.dto.response.ServerAboutResponse;
import ru.tsc.panteleev.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(ServerVersionRequest request);
}
