package ru.tsc.panteleev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @Nullable
    AbstractCommand getCommandByName (String name);

    @Nullable
    AbstractCommand getCommandByArgument (String argument);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

}
