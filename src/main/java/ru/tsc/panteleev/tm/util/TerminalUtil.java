package ru.tsc.panteleev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

    @Nullable
    static Date nextDate() {
        @NotNull final String value = nextLine();
        return DateUtil.toDate(value);
    }

}
