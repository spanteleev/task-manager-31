package ru.tsc.panteleev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.command.data.AbstractDataCommand;
import ru.tsc.panteleev.tm.command.data.DataBackupLoadCommand;
import ru.tsc.panteleev.tm.command.data.DataBackupSaveCommand;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.runWithCommand(DataBackupLoadCommand.NAME, false);
    }

    public void save() {
        bootstrap.runWithCommand(DataBackupSaveCommand.NAME, false);
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save,0,3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

}
