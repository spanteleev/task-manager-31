package ru.tsc.panteleev.tm.component;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.Operation;
import ru.tsc.panteleev.tm.dto.request.AbstractRequest;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;
import ru.tsc.panteleev.tm.task.AbstractServerTask;
import ru.tsc.panteleev.tm.task.ServerAcceptTask;

public class Server {

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final Integer port = bootstrap.getPropertyService().getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void reqistry(
            Class<RQ> reqClass, Operation<RQ, RS> operation
    ) {
        dispatcher.reqistry(reqClass, operation);
    }

    public Object call(AbstractRequest request) {
        return dispatcher.call(request);
    }
}
