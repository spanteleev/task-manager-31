package ru.tsc.panteleev.tm.component;

import ru.tsc.panteleev.tm.api.endpoint.Operation;
import ru.tsc.panteleev.tm.dto.request.AbstractRequest;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void reqistry(
            Class<RQ> reqClass, Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    public Object call(AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }
}
