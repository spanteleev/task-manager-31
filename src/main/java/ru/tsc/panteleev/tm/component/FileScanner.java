package ru.tsc.panteleev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean isCommand = commands.contains(fileName);
            if (isCommand) {
                try {
                    file.delete();
                    bootstrap.runWithCommand(fileName,false);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void start() {
        final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        executorService.scheduleWithFixedDelay(this::process,0,3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

}
