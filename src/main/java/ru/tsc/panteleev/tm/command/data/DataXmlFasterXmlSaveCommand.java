package ru.tsc.panteleev.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.Domain;
import java.io.FileOutputStream;

public class DataXmlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    private static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE IN XML FILE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
