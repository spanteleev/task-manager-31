package ru.tsc.panteleev.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.Domain;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM XML FILE]");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final Domain domain = xmlMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
